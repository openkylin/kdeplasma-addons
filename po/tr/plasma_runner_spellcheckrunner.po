# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# obsoleteman <tulliana@gmail.com>, 2008-2009,2011.
# Volkan Gezer <volkangezer@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons-kde4\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-12-13 00:44+0000\n"
"PO-Revision-Date: 2021-04-20 14:59+0200\n"
"Last-Translator: Volkan Gezer <volkangezer@gmail.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.08.2\n"

#: spellcheck.cpp:96 spellcheck_config.cpp:66 spellcheck_config.cpp:97
#, kde-format
msgid "spell"
msgstr "denetle"

#: spellcheck.cpp:101
#, kde-format
msgctxt ""
"Spelling checking runner syntax, first word is trigger word, e.g.  \"spell\"."
msgid "%1:q:"
msgstr "%1:q:"

#: spellcheck.cpp:102
#, kde-format
msgid "Checks the spelling of :q:."
msgstr ":q: ifadesinin yazımını denetler."

#: spellcheck.cpp:209
#, kde-format
msgctxt "Term is spelled correctly"
msgid "Correct"
msgstr "Doğru"

#: spellcheck.cpp:218
#, kde-format
msgid "Suggested term"
msgstr "Önerilen terim"

#: spellcheck.cpp:227
#, kde-format
msgid "No dictionary found, please install hspell"
msgstr "Sözlük bulunamadı, lütfen hspell yükleyin"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: spellcheck_config.ui:17
#, kde-format
msgid "Spell Check Settings"
msgstr "Yazım Denetimi Ayarları"

#. i18n: ectx: property (text), widget (QCheckBox, m_requireTriggerWord)
#: spellcheck_config.ui:23
#, kde-format
msgid "&Require trigger word"
msgstr "&Tetikleme sözcüğü iste"

#. i18n: ectx: property (text), widget (QLabel, label)
#: spellcheck_config.ui:32
#, kde-format
msgid "&Trigger word:"
msgstr "&Tetikleme sözcüğü:"

#. i18n: ectx: property (text), widget (QPushButton, m_openKcmButton)
#: spellcheck_config.ui:62
#, fuzzy, kde-format
#| msgid "Configure Dictionaries..."
msgid "Configure Dictionaries…"
msgstr "Sözlükleri Yapılandır ..."