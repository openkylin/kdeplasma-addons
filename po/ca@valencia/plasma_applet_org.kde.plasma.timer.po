# Translation of plasma_applet_org.kde.plasma.timer.po to Catalan (Valencian)
# Copyright (C) 2008-2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2 or later.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2008, 2012, 2015, 2018, 2019.
# Joan Maspons <joanmaspons@gmail.com>, 2009.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2014.
# Empar Montoro Martín <montoro_mde@gva.es>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-03-02 03:07+0100\n"
"PO-Revision-Date: 2019-07-18 11:52+0200\n"
"Last-Translator: Empar Montoro Martín <montoro_mde@gva.es>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 19.04.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Aparença"

#: package/contents/config/config.qml:19
#, kde-format
msgctxt "@title"
msgid "Predefined Timers"
msgstr ""

#: package/contents/config/config.qml:24
#, kde-format
msgctxt "@title"
msgid "Advanced"
msgstr "Avançat"

#: package/contents/ui/configAdvanced.qml:22
#, kde-format
msgctxt "@title:label"
msgid "After timer completes:"
msgstr "Després de completar el temporitzador:"

#: package/contents/ui/configAdvanced.qml:26
#, kde-format
msgctxt "@option:check"
msgid "Execute command:"
msgstr "Executa l'ordre:"

#: package/contents/ui/configAppearance.qml:27
#, kde-format
msgctxt "@title:label"
msgid "Display:"
msgstr "Visualització:"

#: package/contents/ui/configAppearance.qml:33
#, kde-format
msgctxt "@option:check"
msgid "Show title:"
msgstr "Mostra el títol:"

#: package/contents/ui/configAppearance.qml:50
#, kde-format
msgctxt "@option:check"
msgid "Show seconds"
msgstr "Mostra els segons"

#: package/contents/ui/configAppearance.qml:62
#, kde-format
msgctxt "@title:label"
msgid "Notifications:"
msgstr "Notificacions:"

#: package/contents/ui/configAppearance.qml:66
#, kde-format
msgctxt "@option:check"
msgid "Show notification text:"
msgstr "Mostra el text de notificació:"

#: package/contents/ui/configTimes.qml:76
#, kde-format
msgid ""
"If you add predefined timers here, they will appear in plasmoid context menu."
msgstr ""

#: package/contents/ui/configTimes.qml:83
#, kde-format
msgid "Add"
msgstr ""

#: package/contents/ui/configTimes.qml:120
#, kde-format
msgid "Scroll over digits to change time"
msgstr ""

#: package/contents/ui/configTimes.qml:126
#, kde-format
msgid "Apply"
msgstr ""

#: package/contents/ui/configTimes.qml:134
#, kde-format
msgid "Cancel"
msgstr ""

#: package/contents/ui/configTimes.qml:143
#, kde-format
msgid "Edit"
msgstr ""

#: package/contents/ui/configTimes.qml:152
#, kde-format
msgid "Delete"
msgstr ""

#: package/contents/ui/main.qml:40 package/contents/ui/main.qml:101
#, kde-format
msgid "Timer"
msgstr "Temporitzador"

#: package/contents/ui/main.qml:45
#, kde-format
msgid "%1 is running"
msgstr "S'està executant %1"

#: package/contents/ui/main.qml:47
#, kde-format
msgid "%1 not running"
msgstr "No s'està executant %1"

#: package/contents/ui/main.qml:51
#, kde-format
msgid "Remaining time left: %1 second"
msgid_plural "Remaining time left: %1 seconds"
msgstr[0] "Temps restant: %1 segon"
msgstr[1] "Temps restant: %1 segons"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Timer finished"
msgstr "El temporitzador ha finalitzat"

#: package/contents/ui/main.qml:119
#, kde-format
msgctxt "@action"
msgid "&Start"
msgstr "&Inicia"

#: package/contents/ui/main.qml:120
#, kde-format
msgctxt "@action"
msgid "S&top"
msgstr "P&ara"

#: package/contents/ui/main.qml:121
#, kde-format
msgctxt "@action"
msgid "&Reset"
msgstr "&Reinicia"

#: package/contents/ui/TimerView.qml:72
#, kde-format
msgid "Timer is running"
msgstr "S'està executant el temporitzador"

#: package/contents/ui/TimerView.qml:72
#, kde-format
msgid ""
"Use mouse wheel to change digits or choose from predefined timers in the "
"context menu"
msgstr ""
"Useu la roda del ratolí per a canviar els dígits o escollir entre els "
"temporitzadors predefinits en el menú contextual"