# Translation of plasma_applet_org.kde.plasma.nightcolorcontrol.po to Catalan (Valencian)
# Copyright (C) 2020-2021 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2020.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-12-13 00:44+0000\n"
"PO-Revision-Date: 2021-12-13 18:28+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 21.12.0\n"

#: package/contents/ui/main.qml:31
#, kde-format
msgid "Night Color Control"
msgstr "Control del color de nit"

#: package/contents/ui/main.qml:34
#, kde-format
msgid "Night Color is inhibited"
msgstr "El color de nit està inhibit"

#: package/contents/ui/main.qml:37
#, kde-format
msgid "Night Color is unavailable"
msgstr "El color de nit no està disponible"

#: package/contents/ui/main.qml:40
#, kde-format
msgid "Night Color is disabled"
msgstr "El color de nit no està desactivat"

#: package/contents/ui/main.qml:43
#, kde-format
msgid "Night Color is not running"
msgstr "El color de nit no està executant-se"

#: package/contents/ui/main.qml:45
#, kde-format
msgid "Night Color is active (%1K)"
msgstr "El color de nit està actiu (%1K)"

#: package/contents/ui/main.qml:94
#, kde-format
msgid "Configure Night Color…"
msgstr "Configura el color de nit…"