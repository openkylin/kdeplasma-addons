# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Kishore G <kishore96@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-12-13 00:44+0000\n"
"PO-Revision-Date: 2021-11-28 19:59+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.11.90\n"

#: package/contents/config/config.qml:11
#, kde-format
msgctxt "@title"
msgid "General"
msgstr "பொதுவானவை"

#: package/contents/ui/configGeneral.qml:27
#, kde-format
msgctxt "@title:label"
msgid "Username style:"
msgstr "பயனர் பெயர் காட்டப்படுவது:"

#: package/contents/ui/configGeneral.qml:30
#, kde-format
msgctxt "@option:radio"
msgid "Full name (if available)"
msgstr "முழுப்பெயர் (கிடைத்தால்)"

#: package/contents/ui/configGeneral.qml:37
#, kde-format
msgctxt "@option:radio"
msgid "Login username"
msgstr "நுழைவுப்பெயர்"

#: package/contents/ui/configGeneral.qml:55
#, kde-format
msgctxt "@title:label"
msgid "Show:"
msgstr "இவற்றை காட்டு:"

#: package/contents/ui/configGeneral.qml:58
#, kde-format
msgctxt "@option:radio"
msgid "Name"
msgstr "பெயர்"

#: package/contents/ui/configGeneral.qml:72
#, kde-format
msgctxt "@option:radio"
msgid "User picture"
msgstr "பயனரின் படம்"

#: package/contents/ui/configGeneral.qml:86
#, kde-format
msgctxt "@option:radio"
msgid "Name and user picture"
msgstr "பெயர் மற்றும் பயனரின் படம்"

#: package/contents/ui/configGeneral.qml:105
#, kde-format
msgctxt "@title:label"
msgid "Advanced:"
msgstr "மேம்பட்டவை:"

#: package/contents/ui/configGeneral.qml:107
#, kde-format
msgctxt "@option:check"
msgid "Show technical session information"
msgstr "அமர்வு குறித்த தொழிற்நுட்ப விவரங்களை காட்டு"

#: package/contents/ui/main.qml:41
#, kde-format
msgid "You are logged in as <b>%1</b>"
msgstr "நீங்கள் <b>%1</b>-ஆக நுழைந்துள்ளீர்"

#: package/contents/ui/main.qml:146
#, kde-format
msgid "Current user"
msgstr "தற்போதைய பயனர்"

#: package/contents/ui/main.qml:169
#, kde-format
msgctxt "Nobody logged in on that session"
msgid "Unused"
msgstr "பயனிலில்லை"

#: package/contents/ui/main.qml:185
#, kde-format
msgctxt "User logged in on console number"
msgid "TTY %1"
msgstr "TTY %1"

#: package/contents/ui/main.qml:187
#, kde-format
msgctxt "User logged in on console (X display number)"
msgid "on %1 (%2)"
msgstr "%1-இல் (%2)"

#: package/contents/ui/main.qml:207
#, kde-format
msgctxt "@action"
msgid "New Session"
msgstr "புதிய அமர்வு"

#: package/contents/ui/main.qml:216
#, kde-format
msgctxt "@action"
msgid "Lock Screen"
msgstr "திரையை பூட்டு"

#: package/contents/ui/main.qml:226
#, fuzzy, kde-format
#| msgctxt "Show a dialog with options to logout/shutdown/restart"
#| msgid "Leave..."
msgctxt "Show a dialog with options to logout/shutdown/restart"
msgid "Leave…"
msgstr "வெளியேறு..."