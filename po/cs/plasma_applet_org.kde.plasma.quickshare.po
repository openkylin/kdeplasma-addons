# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2014, 2015, 2017, 2018.
# Vit Pelcak <vpelcak@suse.cz>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-12-13 00:44+0000\n"
"PO-Revision-Date: 2021-12-22 18:05+0100\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.0\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "General"
msgstr "Obecné"

#: contents/ui/main.qml:211
#, kde-format
msgctxt "@action"
msgid "Paste"
msgstr "Vložit"

#: contents/ui/main.qml:239 contents/ui/main.qml:279
#, kde-format
msgid "Share"
msgstr "Sdílet"

#: contents/ui/main.qml:240 contents/ui/main.qml:280
#, kde-format
msgid "Drop text or an image onto me to upload it to an online service."
msgstr "Upusťte na mě text nebo obrázky pro odeslání na webovou službu."

#: contents/ui/main.qml:280
#, kde-format
msgid "Upload %1 to an online service"
msgstr "Odeslat %1 do služby online"

#: contents/ui/main.qml:293
#, kde-format
msgid "Sending…"
msgstr "Odesílám…"

#: contents/ui/main.qml:294
#, kde-format
msgid "Please wait"
msgstr "Čekejte prosím"

#: contents/ui/main.qml:301
#, kde-format
msgid "Successfully uploaded"
msgstr "Úspěšně odesláno"

#: contents/ui/main.qml:302
#, kde-format
msgid "<a href='%1'>%1</a>"
msgstr "<a href='%1'>%1</a>"

#: contents/ui/main.qml:309
#, kde-format
msgid "Error during upload."
msgstr "Nastala chyba při odesílání."

#: contents/ui/main.qml:310
#, kde-format
msgid "Please, try again."
msgstr "Prosím, zkuste znovu."

#: contents/ui/settingsGeneral.qml:21
#, kde-format
msgctxt "@label:spinbox"
msgid "History size:"
msgstr "Velikost historie:"

#: contents/ui/settingsGeneral.qml:31
#, kde-format
msgctxt "@option:check"
msgid "Copy automatically:"
msgstr "Kopírovat automaticky:"

#: contents/ui/ShareDialog.qml:31
#, kde-format
msgid "Shares for '%1'"
msgstr "Sdílení pro '%1'"

#: contents/ui/ShowUrlDialog.qml:42
#, kde-format
msgid "The URL was just shared"
msgstr "URL byla právě sdílena"

#: contents/ui/ShowUrlDialog.qml:48
#, kde-format
msgctxt "@option:check"
msgid "Don't show this dialog, copy automatically."
msgstr "Nezobrazovat dialog ale kopírovat automaticky."

#: contents/ui/ShowUrlDialog.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Close"
msgstr "Zavřít"