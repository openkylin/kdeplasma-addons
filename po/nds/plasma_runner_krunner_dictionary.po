# translation of plasma_runner_dictionary.po to Low Saxon
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Manfred Wiese <m.j.wiese@web.de>, 2010, 2011, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_dictionary\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-06-16 00:20+0000\n"
"PO-Revision-Date: 2014-03-19 11:26+0100\n"
"Last-Translator: Manfred Wiese <m.j.wiese@web.de>\n"
"Language-Team: Low Saxon <kde-i18n-nds@kde.org>\n"
"Language: nds\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: dictionaryrunner.cpp:32 dictionaryrunner_config.cpp:33
#: dictionaryrunner_config.cpp:51
#, kde-format
msgctxt "Trigger word before word to define"
msgid "define"
msgstr "fastleggen"

#: dictionaryrunner.cpp:39
#, kde-format
msgctxt "Dictionary keyword"
msgid "%1:q:"
msgstr "%1:q:"

#: dictionaryrunner.cpp:39
#, kde-format
msgid "Finds the definition of :q:."
msgstr "Söcht de Verkloren vun :q:."

#: dictionaryrunner_config.cpp:21
#, fuzzy, kde-format
#| msgid "Trigger Word"
msgctxt "@label:textbox"
msgid "Trigger word:"
msgstr "Utlööswoort"